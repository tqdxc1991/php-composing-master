
    
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
    <style  type="text/css"> body {
  
  margin-right: 10%;
  margin-left: 10%;
} </style>
    
    <title>Document</title>
</head>
<body>
    <h1>computer science figures</h1>
<div class="ui three column grid">
<?php
//open the file
$h = fopen("cs_figures.csv", "r");

//get the data
$data = fgetcsv($h, 1000, ",");

//read the date with a loop
while (($data = fgetcsv($h, 1000, ",")) !== FALSE) 
{
    // var_dump($data);
  ?>
  <div class="column">
    <div class="ui fluid card">
    <div class="image">
    <img src="<?php echo $data[5]; ?>">
  </div>

  <div class="content">

    <a class="header"><?php echo $data[0]; ?></a>

    <div class="meta">
      <span class="date"><?php echo $data[1]; ?></span>
    </div>

    <div class="description">
    <?php echo $data[2]; ?>
    </div>

  </div>

  <div class="extra content">
    <a href="<?php echo $data[4]; ?>">
      
    <?php echo $data[3]; ?>
    </a>
  </div>

</div>
</div>

<?php }
 



?>
</div>





</body>
</html>



